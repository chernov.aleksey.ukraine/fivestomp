package com.example.fivestomp.controller;

import com.example.fivestomp.domain.Greeting;
import com.example.fivestomp.domain.HelloMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.util.HtmlUtils;

import java.security.Principal;
import java.util.Random;

@Controller
@Slf4j
public class GreetingController {
    private SimpMessagingTemplate simpMessagingTemplate;

    public GreetingController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greetingAll(HelloMessage message, Principal principal) throws Exception {
        log.info("Incoming message: " + message);
        Thread.sleep(1000); // simulated delay
        //simpMessagingTemplate.convertAndSend("/topic/greetings", new Greeting("hello"));
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }

    @GetMapping("/color")
    @ResponseBody
    public Greeting sendColor(HelloMessage message, Principal principal) throws Exception {
        log.info("Incoming message: " + message);
        Random random = new Random();
        String color = String.format("#%02x%02x%02x", random.nextInt(255), random.nextInt(255), random.nextInt(255));
        simpMessagingTemplate.convertAndSend("/topic/color", new Greeting(color));
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(color));
    }

    @MessageMapping("/hello.user")
    //@SendToUser//("/user/queue/greetings")
    public void greetingToUser(HelloMessage message, Principal principal) throws Exception {
        log.info("Incoming message: " + message);
        Thread.sleep(100); // simulated delay
//        System.out.println("principal.getName() "+ principal.getName());
        simpMessagingTemplate.convertAndSend("/topic/hello.user", new Greeting("hello hello" ));
        Random random = new Random();
        String color = String.format("#%02x%02x%02x", random.nextInt(255), random.nextInt(255), random.nextInt(255));
        simpMessagingTemplate.convertAndSend("/topic/color", new Greeting(color));
        //return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
}
