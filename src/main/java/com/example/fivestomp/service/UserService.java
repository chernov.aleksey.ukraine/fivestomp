package com.example.fivestomp.service;

import com.example.fivestomp.Dao.UserJpaRepository;
import com.example.fivestomp.model.SysUser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserJpaRepository userJpaRepository;

    public Optional<SysUser> getByLogin(@NonNull String login) {

        return userJpaRepository.findUsersByUserName(login);
    }

}