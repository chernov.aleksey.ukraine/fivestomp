package com.example.fivestomp.service;

import com.example.fivestomp.Dao.UserJpaRepository;
import com.example.fivestomp.model.SysUser;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuditorAwareImpl implements AuditorAware<SysUser> {
    @Override
    public Optional<SysUser> getCurrentAuditor() {
        return Optional.empty();
    }

//    private final UserJpaRepository userJpaRepository;
//    @Override
//    public Optional<SysUser> getCurrentAuditor() {
//        // return Optional.of(new SysUser(20L, "Aware", "enc", true, null));
//
//        return Optional.of(userJpaRepository.findUsersByUserName(SecurityContextHolder.getContext()
//                .getAuthentication()
//                .getPrincipal()
//                .toString())
//                .get());
//    }
}
