package com.example.fivestomp.Dao;

import com.example.fivestomp.model.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserJpaRepository extends JpaRepository<SysUser, Long> {
    Optional<SysUser> findUsersByUserName(String userName);

}
