package com.example.fivestomp.Dao;

import com.example.fivestomp.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerJpaRepository extends JpaRepository<Employer, Long> {

}
