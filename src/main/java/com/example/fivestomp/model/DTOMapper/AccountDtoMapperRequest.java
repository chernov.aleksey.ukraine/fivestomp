package com.example.fivestomp.model.DTOMapper;

import com.example.fivestomp.model.Account;
import com.example.fivestomp.model.DTO.AccountDtoRequest;
import com.example.fivestomp.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperRequest extends DtoMapperFacade<Account, AccountDtoRequest> {

    public AccountDtoMapperRequest() {
        super(Account.class, AccountDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Account entity, AccountDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setNumber(dto.getNumber());
        entity.setCurrency(dto.getCurrency());
        entity.setBalance(dto.getBalance());
    }
}
