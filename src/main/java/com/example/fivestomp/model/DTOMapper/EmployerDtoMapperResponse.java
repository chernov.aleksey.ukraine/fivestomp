package com.example.fivestomp.model.DTOMapper;

import com.example.fivestomp.model.*;
import com.example.fivestomp.model.DTO.EmployerDtoResponse;
import com.example.fivestomp.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperResponse extends DtoMapperFacade<Employer, EmployerDtoResponse> {
    public EmployerDtoMapperResponse() {
        super(Employer.class, EmployerDtoResponse.class);
    }

    @Override
    protected void decorateDto(EmployerDtoResponse dto, Employer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setCustomers(entity.getCustomers());
    }
}
