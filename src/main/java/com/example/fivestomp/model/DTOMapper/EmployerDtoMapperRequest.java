package com.example.fivestomp.model.DTOMapper;

import com.example.fivestomp.model.Employer;
import com.example.fivestomp.model.DTO.EmployerDtoRequest;
import com.example.fivestomp.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperRequest extends DtoMapperFacade<Employer, EmployerDtoRequest> {

    public EmployerDtoMapperRequest() {
        super(Employer.class, EmployerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
    }
}
